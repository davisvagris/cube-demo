@extends('layout.main')

@section('container')
    <div class="card">
        <div class="card-header">
            Datu aplūkošana
        </div>
        <div class="card-body">
            <form class="form-inline">
                <div class="form-group mr-3">
                    <label for="eic" class="mr-3">EIC Kods:</label>
                    <input class="form-control" id="eic" name="eic" required value="{{ $eic }}">
                </div>

                <button type="submit" class="btn btn-primary">Aplūkot</button>
            </form>
        </div>
    </div>


    @if ($data->count())
        @foreach ($data as $item)
            <div class="card mt-3">
                <div class="card-header">
                    {{ $item->getObjectName() }} ({{ $item->getObjectAddress() }})
                </div>

                <div class="card-body">
                    @if ($item->getDetails())
                        @foreach ($item->getDetails() as $detail)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Mērījuma punkta numurs</th>
                                        <th>Enerģijas veids</th>
                                        <th>Patēriņa nolasījuma laiks</th>
                                        <th>Lasījuma zona</th>
                                        <th>Lasījuma vērtība</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($detail->getItems() as $row)
                                        <tr>
                                            <td>{{ $item->getMpNr() }}</td>
                                            <td>{{ $detail->getEnTp() }}</td>
                                            <td>{{ $row->date->format('d-m-Y H:i') }}</td>
                                            <td>{{ $row->dayZone }}</td>
                                            <td>{{ $row->cval }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>

                                <tbody>
                                    <tr>
                                        <td colspan="5" class="text-right">Kopējais Patēriņš: {{ $detail->getTotalCval() }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        @endforeach
                    @else
                        <div class="alert alert-info" role="alert">
                            Nevarēja datus ievākt!
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    @endif
@stop