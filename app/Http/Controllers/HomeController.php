<?php

namespace App\Http\Controllers;

use App\Services\Energo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var Energo
     */
    protected $energo;

    /**
     * HomeController constructor.
     * @param Energo $energo
     */
    public function __construct(Energo $energo)
    {
        $this->energo = $energo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $eic = $request->get('eic');
        $data = collect([]);

        if ($eic) {
            $data = $this->energo->getObjectList($eic);
        }

        return view('home', compact('data', 'eic'));
    }
}
