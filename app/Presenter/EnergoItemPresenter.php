<?php

namespace App\Presenter;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class EnergoItemPresenter
{
    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var array
     */
    protected $dayZone = [
        'DAY' => 'Diena',
        'NIGHT' => 'Nakts',
        'DAY_NIGHT' => 'Diennakts',
    ];

    protected $totalCval = 0;

    /**
     * EnergoPresenter constructor.
     *
     * @param $data
     */
    public function __construct(Collection $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getEnTp(): string
    {
        return $this->data->get('enTp');
    }

    /**
     * @return string
     */
    public function getMpNr(): string
    {
        return $this->data->get('mpNr');
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->data->get('cData')->sortByDesc('cDt')->map(function (Collection $data) {
            $cVal = (float) $data->get('cTZData')->get('cVal');
            $this->totalCval += $cVal;
            $dayZone = $data->get('cTZData')->get('cTZ');
            return (Object) [
                'date' => Carbon::parse($data->get('cDt')),
                'cval' => $cVal,
                'dayZone' => $this->dayZone[$dayZone] ?: $dayZone
            ];
        });
    }

    /**
     * @return int
     */
    public function getTotalCval()
    {
        return $this->totalCval;
    }
}