<?php

namespace App\Presenter;

use Illuminate\Support\Collection;

class EnergoPresenter
{
    /**
     * @var Collection
     */
    protected $data;

    /**
     * EnergoPresenter constructor.
     *
     * @param $data
     */
    public function __construct(Collection $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getObjectName(): string
    {
        return $this->data->get('ObjectName');
    }

    /**
     * @return string
     */
    public function getObjectAddress(): string
    {
        return $this->data->get('ObjectAddress');
    }

    /**
     * @return array
     */
    public function getDetails()
    {
        $details = $this->data->get('details');

        if (!$details || !$details && !$details->get('enTCInfo') || !$details->get('enTCInfo')) {
            return [];
        }

        $items = $details->get('enTCInfo')->get('enTCData');

        if (!$items->get('enTp')) {
            return $items->map(function ($item) {
                return new EnergoItemPresenter($item);
            });
        }

        return [
            new EnergoItemPresenter($items)
        ];
    }

    /**
     * @return string
     */
    public function getMpNr()
    {
        return $this->data->get('details')->get('mpNr');
    }
}