<?php

namespace App\Services;

use App\Presenter\EnergoPresenter;
use Artisaninweb\SoapWrapper\Service;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Carbon\Carbon;
use Exception;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Support\Collection;

class Energo
{
    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * @var ConfigRepository
     */
    protected $config;

    /**
     * Energo constructor.
     *
     * @param SoapWrapper $soapWrapper
     * @param ConfigRepository $config
     */
    public function __construct(SoapWrapper $soapWrapper, ConfigRepository $config)
    {
        $this->soapWrapper = $soapWrapper;
        $this->config = $config;

        $this->connect();
    }

    /**
     * Get config from energo config file
     *
     * @param string $name
     * @param null|string $default
     * @return null|string
     */
    protected function getConfig(string $name, ?string $default = null): ?string
    {
        return $this->config->get("energo.{$name}", $default);
    }

    /**
     * Get soap url
     *
     * @param null $path
     * @return string
     */
    protected function getUrl($path = null): string
    {
        $url = $this->getConfig('demo') ?
            $this->getConfig('url.demo') :
            $this->getConfig('url.live');


        if (!is_null($path)) {
            $url .= $path;
        }

        return $url;
    }

    protected function connect()
    {
        $this->soapWrapper->add('main', function (Service $service) {
            $service
                ->wsdl($this->getUrl('/DMBServices/Private/DMBService?wsdl'))
                ->trace(false);
        });

        $this->soapWrapper->add('details', function (Service $service) {
            $service
                ->wsdl($this->getUrl('/DMBAdditionalServices?wsdl'))
                ->trace(false);
        });
    }

    /**
     * Set soap data
     *
     * @return array
     */
    protected function getAuthData()
    {
        return [
            'MMIdentification' => [
                'username' => $this->getConfig('auth.username'),
                'password' => $this->getConfig('auth.password'),
                'supplierEIC' => $this->getConfig('auth.supplierEIC'),
                'messageId' => 'string',
                'apiVersion' => '1.1'
            ],
        ];
    }

    /**
     * @param $name
     * @param $params
     * @param string $server
     * @return mixed
     */
    protected function call($name, $params, $server = 'main')
    {
        try {
            $body = array_merge($this->getAuthData(), $params);

            return $this->soapWrapper->call("{$server}.{$name}", compact('body'));
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get details information
     *
     * @param array $params
     * @return bool|Collection
     */
    public function getDetails(array $params = [])
    {
        $params = array_merge([
            'cEIC' => '',
            'oEIC' => '',
            'dtF' => Carbon::now()->day(1)->timestamp,
            'dtT' => Carbon::now()->timestamp,
            'gr' => 'H',
            'customerPermission' => 'true'
        ], $params);

        $items = $this->call('GetDetailedObjectConsumption', $params,'details');

        if (!$items) {
            return collect([]);
        }

        return collect($items->objCData->mpCData)->recursive();
    }

    /**
     * Get all objects
     *
     * @param string $customerEIC
     * @return bool|Collection
     */
    public function getObjectList(string $customerEIC)
    {
        $items = $this->call('GetObjectList', [
            'RequesterType' => 'I',
            'RequesterEIC' => '',
            'CustomerEIC' => $customerEIC
        ]);

        if (!$items) {
            return collect([]);
        }

        $items = collect($items->Objects->Object)->recursive();

        if ($items->get('ObjectName')) {
            $items['details'] = $this->getDetails([
                'cEIC' => $customerEIC,
                'oEIC' => $items->get('ObjectEIC'),
            ]);

            return collect([
                new EnergoPresenter($items)
            ]);
        }

        return $items->map(function (Collection $row) use($customerEIC) {
            $row['details'] = $this->getDetails([
                'cEIC' => $customerEIC,
                'oEIC' => $row->get('ObjectEIC'),
            ]);

            return new EnergoPresenter($row);
        });
    }
}