<?php

return [
    'demo' => true,
    'url' => [
        'demo' => 'http://test.soa.energo.lv:8011',
        'live' => '',
    ],
    'auth' => [
        'username' => env('ERGO_USERNAME'),
        'password' => env('ERGO_PASSWORD'),
        'supplierEIC' => env('ERGO_SUPPLIEREIC'),
    ]
];